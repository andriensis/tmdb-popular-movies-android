Android native application which retrieves and shows TMDB popular movies.

Architecture: MVVM
Main components: View Model, Navigation Component (graph), Hilt (dependency injection), Retrofit with coroutine support (API calls), Mockk (unit tests mock)

I've added some comments in activity/fragments just to briefly explain the functionality of some pages.