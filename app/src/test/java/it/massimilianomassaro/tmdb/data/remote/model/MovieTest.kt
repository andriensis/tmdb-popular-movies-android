package it.massimilianomassaro.tmdb.data.remote.model

import it.massimilianomassaro.tmdb.fixtures.MoviesFixture
import junit.framework.Assert.assertEquals
import org.junit.Test

class MovieTest {
    @Test
    fun getCoverImageUrl() {
        val movies = MoviesFixture.getMovies()
        assertEquals(movies.first().getCoverImageUrl(), "https://image.tmdb.org/t/p/w500/${movies.first().poster_path}")
    }
}