package it.massimilianomassaro.tmdb.fixtures

import it.massimilianomassaro.tmdb.data.remote.model.Movie


class MoviesFixture {
    companion object {
        fun getMovies(): List<Movie> = listOf(
            Movie(
                1,
                "title",
                "img.png",
                "overview1"
            ),
            Movie(
                2,
                "title2",
                "img2.png",
                "overview2"
            )
        )
    }
}
