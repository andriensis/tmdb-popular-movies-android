package it.massimilianomassaro.tmdb.view.movie_details

import arrow.core.left
import arrow.core.right
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.OverrideMockKs
import io.mockk.impl.annotations.RelaxedMockK
import it.massimilianomassaro.tmdb.PTestRule
import it.massimilianomassaro.tmdb.core.ErrorState
import it.massimilianomassaro.tmdb.core.ServerError
import it.massimilianomassaro.tmdb.core.SuccessState
import it.massimilianomassaro.tmdb.data.remote.api.MoviesApiClient
import it.massimilianomassaro.tmdb.fixtures.MoviesFixture
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class MovieDetailsViewModelTest {
    @get:Rule
    var coroutinesTestRule = PTestRule()

    @RelaxedMockK
    lateinit var moviesApiClient: MoviesApiClient

    @OverrideMockKs
    lateinit var sut: MovieDetailsViewModel

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
    }

    @Test
    fun getMovieDetailsSuccess() {
        val movie = MoviesFixture.getMovies().first()
        coEvery { moviesApiClient.getMovieDetails(any()) }.returns(MoviesFixture.getMovies().first().right())
        sut.getMovieDetails(0)
        assert(sut.state.value is SuccessState)
        assertEquals(sut.movie.value?.id, movie.id)
        assertEquals(sut.movie.value?.title, movie.title)
        assertEquals(sut.movie.value?.overview, movie.overview)
    }

    @Test
    fun getMovieDetailsError() {
        coEvery { moviesApiClient.getMovieDetails(any()) }.returns(ServerError.Generic.left())
        sut.getMovieDetails(0)
        assert(sut.state.value is ErrorState)
        assertEquals(sut.movie.value, null)
    }
}