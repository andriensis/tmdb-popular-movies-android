package it.massimilianomassaro.tmdb.view.movies

import arrow.core.left
import arrow.core.right
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.OverrideMockKs
import io.mockk.impl.annotations.RelaxedMockK
import it.massimilianomassaro.tmdb.PTestRule
import it.massimilianomassaro.tmdb.core.ErrorState
import it.massimilianomassaro.tmdb.core.ServerError
import it.massimilianomassaro.tmdb.core.SuccessState
import it.massimilianomassaro.tmdb.data.remote.api.MoviesApiClient
import it.massimilianomassaro.tmdb.fixtures.MoviesFixture
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class MoviesViewModelTest {
    @get:Rule
    var coroutinesTestRule = PTestRule()

    @RelaxedMockK
    lateinit var moviesApiClient: MoviesApiClient

    @OverrideMockKs
    lateinit var sut: MoviesViewModel

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
    }

    @Test
    fun getMoviesSuccess() {
        coEvery { moviesApiClient.getPopularMovies() }.returns(MoviesFixture.getMovies().right())
        sut.getPopularMovies()
        assert(sut.state.value is SuccessState)
        assertEquals(sut.moviesList.value?.size, MoviesFixture.getMovies().size)
    }

    @Test
    fun getMoviesError() {
        coEvery { moviesApiClient.getPopularMovies() }.returns(ServerError.Generic.left())
        sut.getPopularMovies()
        assert(sut.state.value is ErrorState)
        assertEquals(sut.moviesList.value?.size ?: 0, 0)
    }
}