package it.massimilianomassaro.tmdb.view.movies

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import it.massimilianomassaro.tmdb.R
import it.massimilianomassaro.tmdb.data.remote.model.Movie

class MoviesAdapter(
    private var list: List<Movie>,
    private var onItemClickListener: (movie: Movie) -> Unit,
) : RecyclerView.Adapter<MoviesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val movie: Movie = list[position]
        holder.bind(movie, onItemClickListener)
    }

    override fun getItemCount(): Int = list.size

    fun updateItems(newList: List<Movie>) {
        this.list = newList
        notifyDataSetChanged()
    }

    class ViewHolder(inflater: LayoutInflater, parent: ViewGroup) : RecyclerView.ViewHolder(inflater.inflate(
        R.layout.movie_item, parent, false)) {
        private var movieNameTextView: TextView = itemView.findViewById(R.id.movieNameTextView)

        fun bind(movie: Movie, onItemClickListener: (userAlert: Movie) -> Unit) {
            movieNameTextView.text = movie.title

            itemView.setOnClickListener {
                onItemClickListener(movie)
            }
        }
    }
}

