package it.massimilianomassaro.tmdb.view.movies

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.hilt.navigation.fragment.hiltNavGraphViewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import it.massimilianomassaro.tmdb.R
import it.massimilianomassaro.tmdb.core.*
import it.massimilianomassaro.tmdb.core.extensions.showAlert
import it.massimilianomassaro.tmdb.data.remote.model.Movie
import it.massimilianomassaro.tmdb.databinding.MoviesFragmentBinding
import it.massimilianomassaro.tmdb.view.movie_details.MovieDetailsFragment.Companion.KEY_MOVIE_ID

/**
 Shows the popular movies retried by the API
 **/

@AndroidEntryPoint
class MoviesFragment : Fragment() {

    private val viewModel: MoviesViewModel by hiltNavGraphViewModels(R.id.mobile_navigation)

    private lateinit var _binding: MoviesFragmentBinding
    private val binding get() = _binding

    private lateinit var moviesAdapter: MoviesAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = MoviesFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.state.observe(viewLifecycleOwner, ::handleStateChange)
        viewModel.moviesList.observe(viewLifecycleOwner, ::handleMoviesChange)
        viewModel.getPopularMovies()

        initUi()
    }

    private fun initUi() {
        moviesAdapter = MoviesAdapter(viewModel.moviesList.value.orEmpty()) {
            val bundle = bundleOf(KEY_MOVIE_ID to it.id)
            findNavController().navigate(R.id.action_nav_movies_to_nav_movie_details, bundle)
        }
        binding.moviesRecyclerView.apply {
            adapter = moviesAdapter
        }
    }

    private fun handleStateChange(viewModelState: ViewModelState) {
        when(viewModelState) {
            is LoadingState -> {
                binding.spinnerGroup.visibility = View.VISIBLE
                binding.moviesRecyclerView.visibility = View.GONE
            }
            is SuccessState -> {
                binding.spinnerGroup.visibility = View.GONE
                binding.moviesRecyclerView.visibility = View.VISIBLE
            }
            is ErrorState -> {
                binding.spinnerGroup.visibility = View.GONE
                binding.moviesRecyclerView.visibility = View.GONE

                // Just a basic alert. Allow the user to keep using the app
                when(viewModelState.error) {
                    is NetworkNotAvailableError -> showAlert(getString(R.string.error), getString(R.string.network_error))
                    else -> showAlert(getString(R.string.error), getString(R.string.error_message_generic))
                }
            }
        }
    }

    private fun handleMoviesChange(moviesList: List<Movie>) = moviesAdapter.updateItems(moviesList)
}