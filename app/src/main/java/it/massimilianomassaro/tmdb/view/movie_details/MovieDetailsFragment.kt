package it.massimilianomassaro.tmdb.view.movie_details

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.hilt.navigation.fragment.hiltNavGraphViewModels
import androidx.navigation.fragment.findNavController
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint
import it.massimilianomassaro.tmdb.R
import it.massimilianomassaro.tmdb.core.*
import it.massimilianomassaro.tmdb.core.extensions.showAlert
import it.massimilianomassaro.tmdb.data.remote.model.Movie
import it.massimilianomassaro.tmdb.data.remote.model.getCoverImageUrl
import it.massimilianomassaro.tmdb.databinding.MovieDetailsFragmentBinding
import it.massimilianomassaro.tmdb.databinding.MoviesFragmentBinding
import timber.log.Timber

/**
 Shows the movie details
 I've added a new API request even though we already have all needed details from the previous API call (so I could also have passed the entire object to the fragment in a bundle)
 **/

@AndroidEntryPoint
class MovieDetailsFragment : Fragment() {

    private val viewModel: MovieDetailsViewModel by hiltNavGraphViewModels(R.id.mobile_navigation)

    private lateinit var _binding: MovieDetailsFragmentBinding
    private val binding get() = _binding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = MovieDetailsFragmentBinding.inflate(inflater, container, false)

        arguments?.getInt(KEY_MOVIE_ID)?.let {
            viewModel.getMovieDetails(it)
        } ?: run {
            showAlert(
                getString(R.string.error),
                getString(R.string.error_message_generic)
            ) { findNavController().navigateUp() }
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.state.observe(viewLifecycleOwner, ::handleStateChange)
        viewModel.movie.observe(viewLifecycleOwner, ::handleMovieChange)
    }

    private fun handleStateChange(viewModelState: ViewModelState) {
        when(viewModelState) {
            is LoadingState -> {
                binding.spinnerGroup.visibility = View.VISIBLE
                binding.cardView.visibility = View.GONE
            }
            is SuccessState -> {
                binding.spinnerGroup.visibility = View.GONE
                binding.cardView.visibility = View.VISIBLE
            }
            is ErrorState -> {
                binding.spinnerGroup.visibility = View.GONE
                binding.cardView.visibility = View.GONE

                // Just a basic alert. Allow the user to keep using the app
                when(viewModelState.error) {
                    is NetworkNotAvailableError -> showAlert(getString(R.string.error), getString(R.string.network_error))
                    else -> showAlert(getString(R.string.error), getString(R.string.error_message_generic))
                }
            }
        }
    }

    private fun handleMovieChange(movie: Movie) {
        Timber.d(movie.getCoverImageUrl())
        Picasso.get()
            .load(movie.getCoverImageUrl())
            .placeholder(R.drawable.ic_baseline_image_24)
            .into(binding.movieImageView)

        binding.movieNameTexView.text = movie.title
        binding.movieOverviewTextView.text = movie.overview
    }

    companion object {
        const val KEY_MOVIE_ID = "MOVIE_ID"
    }
}