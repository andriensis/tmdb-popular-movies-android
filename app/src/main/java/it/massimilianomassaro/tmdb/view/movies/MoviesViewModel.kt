package it.massimilianomassaro.tmdb.view.movies

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import it.massimilianomassaro.tmdb.core.*
import it.massimilianomassaro.tmdb.data.remote.api.MoviesApiClient
import it.massimilianomassaro.tmdb.data.remote.model.Movie
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class MoviesViewModel
@Inject constructor(
    private val moviesApiClient: MoviesApiClient
): ViewModel() {
    private val _state = MutableLiveData<ViewModelState>()
    val state: LiveData<ViewModelState> = _state

    private val _movieslist = MutableLiveData<List<Movie>>()
    val moviesList: LiveData<List<Movie>> = _movieslist

    fun getPopularMovies() {
        _state.value = LoadingState
        viewModelScope.launch {
            moviesApiClient.getPopularMovies().fold(
                ifLeft = {
                    Timber.e("Error while getting movies -> $it")
                    _state.value = ErrorState(it)
                },
                ifRight = {
                    _state.value = SuccessState()
                    _movieslist.value = it
                }
            )
        }
    }
}