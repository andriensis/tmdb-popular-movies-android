package it.massimilianomassaro.tmdb.data.remote.api

import arrow.core.Either
import it.massimilianomassaro.tmdb.core.PError
import it.massimilianomassaro.tmdb.data.remote.BaseRetrofitClient
import it.massimilianomassaro.tmdb.data.remote.mapResponse
import it.massimilianomassaro.tmdb.data.remote.model.Movie
import it.massimilianomassaro.tmdb.data.remote.model.response.MoviesResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import javax.inject.Inject

interface MoviesApi {
    @GET("movie/popular")
    suspend fun getPopularMovies(): Response<MoviesResponse>

    @GET("movie/{movieId}")
    suspend fun getMovieDetails(
        @Path("movieId") movieId: Int
    ): Response<Movie>
}

class MoviesApiClient
@Inject constructor(
    private val client: MoviesApi
) : BaseRetrofitClient() {

    suspend fun getPopularMovies(): Either<PError, List<Movie>> {
        return call { client.getPopularMovies() }.mapResponse().map { it.results }
    }

    suspend fun getMovieDetails(movieId: Int): Either<PError, Movie> {
        return call { client.getMovieDetails(movieId) }.mapResponse()
    }
}