package it.massimilianomassaro.tmdb.data.remote.model.response

import it.massimilianomassaro.tmdb.data.remote.model.Movie

data class MoviesResponse(
    val results: List<Movie>
)