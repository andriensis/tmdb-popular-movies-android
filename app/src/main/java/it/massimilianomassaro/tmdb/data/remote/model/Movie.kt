package it.massimilianomassaro.tmdb.data.remote.model

import it.massimilianomassaro.tmdb.BuildConfig


data class Movie(
    val id: Int,
    val title: String,
    val poster_path: String,
    val overview: String
)

fun Movie.getCoverImageUrl() = "${BuildConfig.IMG_BASE_URL}${this.poster_path}"