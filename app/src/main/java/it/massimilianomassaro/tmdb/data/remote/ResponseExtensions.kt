package it.massimilianomassaro.tmdb.data.remote

import arrow.core.Either
import arrow.core.flatMap
import arrow.core.left
import arrow.core.right
import it.massimilianomassaro.tmdb.core.PError
import it.massimilianomassaro.tmdb.core.ServerError
import retrofit2.Response

internal fun <T> Either<PError, Response<T>>.mapResponse(): Either<PError, T> =
    this.flatMap { response ->
        if (response.isSuccessful) {
            response.body()!!.right()
        } else {
            // We could use specific errors according to the backend response
            ServerError.Generic.left()
        }
    }