package it.massimilianomassaro.tmdb.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import it.massimilianomassaro.tmdb.data.remote.api.MoviesApi
import retrofit2.Retrofit

@Module
@InstallIn(SingletonComponent::class)
object RemoteRepositoryModule {

    @Provides
    fun provideMoviesApi(retrofit: Retrofit): MoviesApi {
        return retrofit.create(MoviesApi::class.java)
    }
}