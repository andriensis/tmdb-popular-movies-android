package it.massimilianomassaro.tmdb.core

sealed class ViewModelState
data class SuccessState(val result: Any? = null): ViewModelState()
object LoadingState: ViewModelState()
data class ErrorState(val error: PError? = null): ViewModelState()


