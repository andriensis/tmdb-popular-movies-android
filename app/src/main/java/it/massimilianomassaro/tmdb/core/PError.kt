package it.massimilianomassaro.tmdb.core

sealed class PError

sealed class ServerError : PError() {
    object Generic : ServerError()
}

object NetworkNotAvailableError: PError()