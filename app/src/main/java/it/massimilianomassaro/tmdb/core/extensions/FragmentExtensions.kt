package it.massimilianomassaro.tmdb.core.extensions

import android.app.AlertDialog
import androidx.fragment.app.Fragment
import it.massimilianomassaro.tmdb.R

fun Fragment.showAlert(title: String, message: String, onDismiss: (() -> Unit)? = null) {
    val alertDialogBuilder = AlertDialog.Builder(context)
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton(getString(R.string.ok), null)

    onDismiss?.let {
        alertDialogBuilder.setOnDismissListener {
            onDismiss()
        }
    }

    alertDialogBuilder.show()
}